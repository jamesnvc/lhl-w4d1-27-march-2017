//
//  GithubRepo.m
//  networkingdemo
//
//  Created by James Cash on 27-03-17.
//  Copyright © 2017 Occasionally Cogent. All rights reserved.
//

#import "GithubRepo.h"

@implementation GithubRepo

- (instancetype)initWithDict:(NSDictionary *)infoDict
{
    self = [super init];
    if (self) {
        _name = infoDict[@"name"];
        _repoDescription = infoDict[@"description"];
        _language = infoDict[@"language"];
    }
    return self;
}

@end
