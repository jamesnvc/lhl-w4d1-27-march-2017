//
//  ViewController.m
//  networkingdemo
//
//  Created by James Cash on 27-03-17.
//  Copyright © 2017 Occasionally Cogent. All rights reserved.
//

#import "ViewController.h"
#import "GithubRepo.h"

@interface ViewController () <UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UITextField *userNameTextField;

@property (nonatomic,strong) NSURL *apiURL;
@property (nonatomic,strong) NSArray<GithubRepo*>* repos;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.apiURL = [NSURL URLWithString:@"https://api.github.com/users/jamesnvc/repos"];

    [self fetchGithubData];
}

- (void)fetchGithubData {
    NSURLSessionTask *task =
    [[NSURLSession sharedSession]
     dataTaskWithURL:self.apiURL
     completionHandler:^(NSData * data, NSURLResponse *  response, NSError * error) {
         if (error) {
             NSLog(@"An error occured making request: %@", error.localizedDescription);
             return;
         }
         NSLog(@"Response had MIME type %@", response.MIMEType);
         NSError *err = nil;
         NSArray<NSDictionary*> *responseInfo = [NSJSONSerialization
                                                 JSONObjectWithData:data
                                                 options:0
                                                 error:&err];
         if (err) {
             NSLog(@"Something went wrong parsing JSON: %@", err.localizedDescription);
             return;
         }
         NSMutableArray *repos = [@[] mutableCopy];
         for (NSDictionary *dict in responseInfo) {
             GithubRepo *r = [[GithubRepo alloc] initWithDict:dict];
             [repos addObject:r];
         }
         self.repos = repos;
         [[NSOperationQueue mainQueue] addOperationWithBlock:^{
             [self.tableView reloadData];
         }];
     }];
    [task resume];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.repos.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"repoCell"];

    GithubRepo *repo = self.repos[indexPath.row];

    UILabel *label = [cell viewWithTag:1];

    label.text = [NSString stringWithFormat:@"%@ (written in %@)", repo.name, repo.language];

    return cell;
}

- (IBAction)reloadGithubData:(id)sender {
    NSString *userName = self.userNameTextField.text;
    self.apiURL = [NSURL URLWithString:[NSString stringWithFormat:@"https://api.github.com/users/%@/repos", userName]];
    [self fetchGithubData];
}

@end
