//
//  GithubRepo.h
//  networkingdemo
//
//  Created by James Cash on 27-03-17.
//  Copyright © 2017 Occasionally Cogent. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GithubRepo : NSObject

@property (strong,nonatomic) NSString *name;
@property (strong,nonatomic) NSString *repoDescription;
@property (strong,nonatomic) NSString *language;

- (instancetype)initWithDict:(NSDictionary*)infoDict;

@end
